package fr.cesi.forge ; 

import org.slf4j.Logger; 
import org.slf4j.LoggerFactory; 
import fr.cesi.forge.GereMessage;

/**
 * Ceci est la classe principale.
 * 
 * @author 2129158
 *
 */
public class Hello { 

	private static final Logger logger = LoggerFactory.getLogger(Hello.class); 
/**
 * Point d'entree.
 * 
 * R�alise pendant le cours sur l'integration continue.
 * 
 * 
 * @param args
 */
	public static void main( String [] args ){
		
		


		logger.debug("Avant le message"); 

		
		GereMessage maVar;
		maVar = new GereMessage();
		
		String bonjours = maVar.gereMessage("msg");
		System.out.println(bonjours);
		logger.debug("Apres le message"); 

	} 

} 

